import bluetooth as bt
import websocket
import json
import time

node_id = "c2ZzZGZzZnNmc2RmZ2hyc2RmZWZzZg=="  # 30 chars long, base64 compliant
node_token = "12345678901234567890123456789012"  # 32 chars long
server_uri = "ws://127.0.0.1:7070/ws"
sleepTime = 30  # how long the node waits between scans
scantime = 10  # how long to scan for (seconds)

try:
    from bluetooth.ble import DiscoveryService
except ImportError:
    DiscoveryService = None


def bluetooth_classic_scan():  # Only works if devices are in pairing mode
    return bt.discover_devices(duration=scantime, flush_cache=True, lookup_names=True)


def bluetooth_low_energy_scan():  # Linux only
    if DiscoveryService is None:
        return None

    svc = DiscoveryService()
    return svc.discover(scantime)


class Data:
    ble: list
    bt: list


class Response:
    type: str
    id: str
    data: dict


def on_message(ws, message):
    print("Got message from server")


def on_error(ws, error):
    print(error)
    ws.close()
    time.sleep(30)  # wait 30 sec before reestablishing connection
    connect()


def on_close(ws, close_status_code, close_msg):
    print("### closed ###")


def on_open(ws):
    print("ws opened")

    while True:
        print("Looking for bt/ble devices...")

        device_data = Data()

        dev_classic = bluetooth_classic_scan()
        if dev_classic:
            print("classic bt devices found!")
            # only send bssids
            classic_bssids: list = []
            for address, name in dev_classic:
                classic_bssids.append(address)
            device_data.bt = classic_bssids

        dev_ble = bluetooth_low_energy_scan()
        if dev_ble:
            print("ble devices found!")
            # only send bssids
            ble_bssids: list = []
            for address, name in dev_ble.items():
                ble_bssids.append(address)
            device_data.ble = ble_bssids

        res = Response()

        res.data = device_data.__dict__
        res.id = node_id
        res.type = "measurement"
        ws.send(json.dumps(res.__dict__))
        time.sleep(sleepTime)


def connect():
    if __name__ == "__main__":
        websocket.enableTrace(True)
        ws = websocket.WebSocketApp(server_uri,
                                    header={"Client-ID": node_id, "Authorization": node_token},
                                    on_open=on_open,
                                    on_message=on_message,
                                    on_error=on_error,
                                    on_close=on_close
                                    )

        ws.run_forever()


connect()
